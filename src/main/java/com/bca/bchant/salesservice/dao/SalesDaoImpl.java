package com.bca.bchant.salesservice.dao;

import java.security.MessageDigest;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.bca.bchant.salesservice.dbconn.getDbConnection;
import com.bca.bchant.salesservice.model.AccessTokenResponse;
import com.bca.bchant.salesservice.model.ErrorSchema;
import com.bca.bchant.salesservice.model.FirebaseResponse;
import com.bca.bchant.salesservice.model.IDSResponse;
import com.bca.bchant.salesservice.model.ListSales;
import com.bca.bchant.salesservice.model.TotalSales;
import com.bca.bchant.salesservice.model.Sales;
import com.google.gson.Gson;

import oracle.jdbc.OracleTypes;

@Repository
public class SalesDaoImpl implements SalesDao{

	private static final Logger LOGGER = LoggerFactory.getLogger(SalesDaoImpl.class);

	Connection conn = null;
	CallableStatement callableStatement = null;
	PreparedStatement pSt = null;
	PreparedStatement pSt2 = null;
	PreparedStatement pSt3 = null;
	PreparedStatement pSt4 = null;
	PreparedStatement pSt5 = null;
	PreparedStatement pSt6 = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	ResultSet rs3 = null;
	ResultSet rs4 = null;
	ResultSet rs5 = null;
	ResultSet rs6 = null;
	
	@Value("${sales.clientId}")
	String clientId;
	
	@Value("${sales.apiKey}")
	String apiKey;
	
	@Value("${sales.url}")
	String url;
	
	@Value("${sales.authorization}")
	String authorization;
	
	@Value("${sales.grantType}")
	String grantType;
	
	@Value("${sales.query}")
	String query;
	
	@Value("${sales.query2}")
	String query2;
	
	@Value("${sales.query3}")
	String query3;
	
	@Value("${sales.query4}")
	String query4;
	
	@Value("${sales.balanceInquiryTrxCdValue}")
	String balanceInquiryTrxCdValue;
	
	@Value("${sales.keyUserIdValue}")
	String keyUserIdValue;
	
	@Value("${sales.urlBalanceInquiry}")
	String urlBalanceInquiry;
	
	@Value("${sales.signatureHash}")
	String signatureHash;
	
	@Value("${sales.transferTrxCdValue}")
	String transferTrxCd;
	
	@Value("${sales.transferCicsTxnValue}")
	String transferCicsTxn;
	
	@Value("${sales.transferWsIdValue}")
	String transferWsId;
	
	@Value("${sales.transferTxnCodeValue}")
	String transferTxnCode;
	
	@Value("${sales.transferTxnRefNoValue}")
	String transferTxnRefNo;
	
	@Value("${sales.transferLogTypeValue}")
	String transferLogType;
	
	@Value("${sales.transferAmtKursValue}")
	String transferAmtKurs;
	
	@Value("${sales.transferAmtChargesValue}")
	String transferAmtCharges;
	
	@Value("${sales.transferCurrCodeValue}")
	String transferCurrCode;
	
	@Value("${sales.transferServBranchValue}")
	String transferServBranch;
	
	@Value("${sales.urlTransfer}")
	String urlTransfer;
	
	@Value("${sales.urlFirebase}")
	String urlFirebase;
	
	@Value("${sales.serverKeyFirebase}")
	String keyFirebase;
	
	@Value("${sales.urlFirebaseFailed}")
	String urlFirebaseFailed;
	
	@Value("${sales.queryGetSalesDetail}")
	String queryGetSalesDetail;
	
	@Value("${sales.queryGetSalesDetail2}")
	String queryGetSalesDetail2;
	
	@Value("${sales.queryGetSalesStatus}")
	String queryGetSalesStatus;
	
	@Value("${sales.queryGetSalesName}")
	String queryGetSalesName;
	
	@Value("${sales.queryTest}")
	String queryTest;
	
	public static String hMacRequestString(String requestString, String key, String algorithm) throws Exception {
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);
        
        Mac hmac = Mac.getInstance(algorithm);
        hmac.init(secretKey);
        byte[] hmacAsBytes = hmac.doFinal(requestString.getBytes("UTF-8"));
        
        return new String(Hex.encodeHex(hmacAsBytes)).toLowerCase();
    }
	
	@Override
	public ErrorSchema setSales(String trxId, String customerId, String browserKey) throws SQLException {
		ErrorSchema errorSchema = new ErrorSchema();
		
		Date dateNow = Calendar.getInstance().getTime();
		StringBuilder sb = new StringBuilder();
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(dateNow);
		String timeStampTransfer = new SimpleDateFormat("ddMMyyyy").format(dateNow);
/*		String clientId = "7AC5C6738ED81CFCE05400144FFA3B5D";
		String apiKey = "58867dad-3387-44ef-a84b-30b342b1f8f9";
		
		String authorization = "";

		*/
		String accessToken = "";
		String totalHarga = "";
		String merchantId = "";
		String rekMerchant = "";
		String rekCustomer = "";
		String bodyBalanceInquiry = "";
		String bodyTransfer = "";
		String balanceCustomer = "";
		RestTemplate rest = new RestTemplate();
		//String url = "https://devapi.klikbca.com/api/oauth/token";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Authorization", authorization);
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("grant_type", grantType);
		
		HttpEntity<MultiValueMap<String, String>> headerG = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<AccessTokenResponse> responseAccessToken = rest.postForEntity(url, headerG , AccessTokenResponse.class);
		LOGGER.info("masuk restpoforentity");
		LOGGER.info("accesstoken : " + responseAccessToken.getBody().getAccess_token());
		accessToken = responseAccessToken.getBody().getAccess_token();
		String authorization2 = "Bearer " + accessToken;
		
		/*String query = "SELECT SUM(total) AS jumlah_total, merchantid "
				+ "FROM sales WHERE trxid = ? GROUP BY merchantid ";
		
		String query2 = "SELECT rekmerchant "
				+ "FROM merchant WHERE merchantid = ? ";
		
		String query3 = "SELECT rekcustomer "
				+ "FROM customer WHERE customerid = ? ";
		
		String query4 = "SELECT s.productid AS productid, p.qty AS stock, s.qty AS qtyjual "
				+ "FROM sales s "
				+ "JOIN product p ON p.productid = s.productid "
				+ "WHERE trxid = ? ";*/
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			conn.setAutoCommit(false);
			
			//PreparedStatement
			String status = "";
			
			LOGGER.info("Siap menjalankan query " + queryGetSalesStatus + " ...");
			
			pSt6 = conn.prepareStatement(queryGetSalesStatus);
			LOGGER.info("Prepared Statement dengan isi " + queryGetSalesStatus + " berhasil dijalanakan ...");
			
			pSt6.setString(1, trxId);
			
			rs6 = pSt6.executeQuery();
	
			while(rs6.next()){
				status = rs6.getString("status");
			}
			LOGGER.info("Berhasil mendapatkan Status : " + status);
			if(status.equals("1")){
				throw new Exception("Transaksi gagal karena transaksi dengan transaksi ID : " + trxId + " sudah dibayar, ");
			}
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query4 + " ...");
			
			pSt5 = conn.prepareStatement(query4);
			LOGGER.info("Prepared Statement dengan isi " + query4 + " berhasil dijalanakan ...");
			
			pSt5.setString(1, trxId);
			LOGGER.info("TRX ID QUERY 5 -> " + trxId);
			rs5 = pSt5.executeQuery();
			
			String productId = "";
			Integer stock;
			Integer stockAkhir;
			Integer qtyJual;
			while(rs5.next()){
				productId = rs5.getString("productid");
				if(!rs5.getString("stock").equals("-")){
					stock = Integer.parseInt(rs5.getString("stock"));
					qtyJual = Integer.parseInt(rs5.getString("qtyjual"));
					stockAkhir = stock - qtyJual;
					
					if(stockAkhir < 0) throw new Exception("Transaksi gagal karena stock productID " + productId + " dengan jumlah " + stock + " tidak mencukupi dengan penjualan sejumlah " + qtyJual +", ");
				}
			}
			
			LOGGER.info("Berhasil mendapatkan data jumlah total transaksi : " + totalHarga);
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query + " ...");
			
			pSt = conn.prepareStatement(query);
			LOGGER.info("Prepared Statement dengan isi " + query + " berhasil dijalanakan ...");
			
			pSt.setString(1, trxId);
			LOGGER.info("TRX ID DI ATAS -> " + trxId);
			rs = pSt.executeQuery();
			
			while(rs.next()){
				totalHarga = rs.getString("jumlah_total");
				merchantId = rs.getString("merchantid");
			}
			
			LOGGER.info("Berhasil mendapatkan data jumlah total transaksi : " + totalHarga);
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query2 + " ...");
			
			pSt2 = conn.prepareStatement(query2);
			LOGGER.info("Prepared Statement dengan isi " + query2 + " berhasil dijalanakan ...");
			
			pSt2.setString(1, merchantId);
			
			rs2 = pSt2.executeQuery();
			
			while(rs2.next()){
				rekMerchant = rs2.getString("rekmerchant");
			}
			
			LOGGER.info("Berhasil mendapatkan data rekening merchant : " + rekMerchant);
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + query3 + " ...");
			
			pSt3 = conn.prepareStatement(query3);
			LOGGER.info("Prepared Statement dengan isi " + query3 + " berhasil dijalanakan ...");
			
			pSt3.setString(1, customerId);
			
			rs3 = pSt3.executeQuery();
			
			while(rs3.next()){
				rekCustomer = rs3.getString("rekcustomer");
			}
			
			LOGGER.info("Berhasil mendapatkan data rekening customer : " + rekCustomer);
			
			//-----------------------------------------------------UPDATE STOK & STATUS---------------------------------------------
			
			String spName = "UPDATE_SALES_STATUS";
			String errorMessage = "";
			Integer errorMessageNumb = 0;
			
			//StoredProcedure
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
			
			callableStatement.setString(1, customerId);
			callableStatement.setString(2, trxId);
			callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
	
			rs = (ResultSet) callableStatement.getObject(3);
			while(rs.next()){
				errorMessageNumb = rs.getInt("v_error_msg_numb");
				errorMessage = rs.getString("v_error_msg");
			}
			
			if(errorMessageNumb == 1) {
				//errorSchema.setErrorCode("ESB-00-00");
				//errorSchema.setErrorMessage(errorMessage);
				LOGGER.info("Berhasil update status sales dengan id trx " + trxId + " ...");
				
				//PreparedStatement
				LOGGER.info("Siap menjalankan query " + query4 + " ...");
				
				pSt4 = conn.prepareStatement(query4);
				LOGGER.info("Prepared Statement dengan isi " + query4 + " berhasil dijalanakan ...");
				
				pSt4.setString(1, trxId);
				
				rs4 = pSt4.executeQuery();
				
				while(rs4.next()){
					productId = rs4.getString("productid");
					if(!rs4.getString("stock").equals("-")){
						stock = Integer.parseInt(rs4.getString("stock"));
						qtyJual = Integer.parseInt(rs4.getString("qtyjual"));
						stockAkhir = stock - qtyJual;
						
						spName = "UPDATE_SALES_STOCK";
						errorMessage = "";
						errorMessageNumb = 0;
						
						//StoredProcedure
						LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
						
						callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
						LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
						
						callableStatement.setString(1, productId);
						callableStatement.setString(2, stockAkhir.toString());
						callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
						
						callableStatement.executeUpdate();
						LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
				
						rs = (ResultSet) callableStatement.getObject(3);
						while(rs.next()){
							errorMessageNumb = rs.getInt("v_error_msg_numb");
							errorMessage = rs.getString("v_error_msg");
						}
						
						if(errorMessageNumb == 1){
							//errorSchema.setErrorCode("ESB-00-000");
							//errorSchema.setErrorMessage("Berhasil update stock product " + productId + " menjadi " + stock.toString());
							LOGGER.error("Berhasil update stock product " + productId + " menjadi " + stockAkhir + " dari " + stock + ", ");
						}
						else{
							throw new Exception("Gagal melakukan update stock, ");
						}
					}
				}
				
			}
			else {
				throw new Exception("Gagal melakukan update status sales, ");
			}
			
			//-----------------------------------------BALANCEINQUIRY & TRANSFER------------------------------------------------------------------------
			
			bodyBalanceInquiry = "{\"TrxCd\":\""+balanceInquiryTrxCdValue+"\",\"MainData\":[{\"Key\":\"accountNumber\",\"Value\":\"" + rekCustomer + "\"},{\"Key\":\"userId\",\"Value\":\""+keyUserIdValue+"\"}],\"AdditionalData\":[]}";
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(bodyBalanceInquiry.getBytes("UTF-8"));

			String hashedRequestBody = new String(Hex.encodeHex(digest.digest()));
			
			String sb2 = sb.append("POST").append(":").append(urlBalanceInquiry).append(":").append(accessToken).append(":").append(hashedRequestBody).append(":").append(timeStamp).toString();

			String signature = hMacRequestString(sb2, apiKey, signatureHash);
			
			//url = urlBalanceInquiry;
			headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			headers.add("Authorization", authorization2);
			headers.add("X-BCA-ClientID", clientId);
			headers.add("X-BCA-Key", apiKey);
			headers.add("X-BCA-Signature", signature);
			headers.add("X-BCA-Timestamp", timeStamp);
			
			rest = new RestTemplate();
			HttpEntity<String> entity = new HttpEntity<String>(bodyBalanceInquiry, headers);
			String response = rest.postForObject(urlBalanceInquiry, entity,String.class);
			LOGGER.info("response body Balance Inquiry -> " + response);
			
			Gson gson = new Gson(); // Or use new GsonBuilder().create();
			IDSResponse responseBalanceInquiry = gson.fromJson(response, IDSResponse.class);
			
			for(int i = 0; i < responseBalanceInquiry.getMainData().size(); i++){
				if(responseBalanceInquiry.getMainData().get(i).getKey().equals("availableBalance")){
					balanceCustomer = responseBalanceInquiry.getMainData().get(i).getValue();
					LOGGER.info("Berhasil Inquiry Balance Customer");
				}
				else if(responseBalanceInquiry.getMainData().get(i).getKey().equals("errorMessage")){
					throw new Exception(responseBalanceInquiry.getMainData().get(i).getValue() + ", ");
				}
			}

			balanceCustomer = balanceCustomer.replaceFirst("^0+(?!$)", "");
			balanceCustomer = balanceCustomer.substring(0,balanceCustomer.length()-1);
			LOGGER.info("balanceCustomer : " + balanceCustomer);
			LOGGER.info("TotalHarga : " + Long.parseLong(totalHarga) * 100);
			
			if(Long.parseLong(balanceCustomer) < (Long.parseLong(totalHarga) * 100)){
				throw new Exception("Balance Customer tidak cukup, ");
			}
			else{
				bodyTransfer = "{\"TrxCd\":\""+transferTrxCd+"\"," + 
								"\"MainData\":" + 
								"[{\"Key\":\"cicsTxn\",\"Value\":\""+transferCicsTxn+"\"}," + 
								 "{\"Key\":\"wsid\",\"Value\":\""+transferWsId+"\"}," + 
								"{\"Key\":\"txnCode\",\"Value\":\""+transferTxnCode+"\"}," + 
								"{\"Key\":\"txnRefNo\",\"Value\":\""+transferTxnRefNo+"\"}," + 
								"{\"Key\":\"acctNoFrom\",\"Value\":\""+ rekCustomer +"\"}," + 
								"{\"Key\":\"acctNoTo\",\"Value\":\""+ rekMerchant +"\"}," + 
								"{\"Key\":\"logType\",\"Value\":\""+transferLogType+"\"}," + 
								"{\"Key\":\"amtExchange\",\"Value\":\""+ totalHarga +"00\"}," + 
								"{\"Key\":\"amtInput\",\"Value\":\""+ totalHarga +"00\"}," + 
								"{\"Key\":\"amtKurs\",\"Value\":\""+transferAmtKurs+"\"}," + 
								"{\"Key\":\"amtCharges\",\"Value\":\""+transferAmtCharges+"\"}," + 
								"{\"Key\":\"currCode\",\"Value\":\""+transferCurrCode+"\"}," + 
								"{\"Key\":\"busnDate\",\"Value\":\""+ timeStampTransfer +"\"}," + 
								"{\"Key\":\"servBranch\",\"Value\":\""+transferServBranch+"\"}," + 
								"{\"Key\":\"desc1\",\"Value\":\"\"}," + 
								"{\"Key\":\"desc2\",\"Value\":\"\"}," + 
								"{\"Key\":\"desc3\",\"Value\":\"\"}," + 
								"{\"Key\":\"desc4\",\"Value\":\"\"}," + 
								"{\"Key\":\"desc5\",\"Value\":\"\"}]," + 
							"\"AdditionalData\":[]}";
				digest.reset();
				digest = MessageDigest.getInstance("SHA-256");
				digest.update(bodyTransfer.getBytes("UTF-8"));

				hashedRequestBody = new String(Hex.encodeHex(digest.digest()));
				
				sb = new StringBuilder();
				sb2 = sb.append("POST").append(":").append(urlTransfer).append(":").append(accessToken).append(":").append(hashedRequestBody).append(":").append(timeStamp).toString();

				signature = hMacRequestString(sb2, apiKey, "HmacSHA256");
				
				//url = urlTransfer;
				headers = new HttpHeaders();
				headers.add("Content-Type", "application/json");
				headers.add("Authorization", authorization2);
				headers.add("X-BCA-ClientID", clientId);
				headers.add("X-BCA-Key", apiKey);
				headers.add("X-BCA-Signature", signature);
				headers.add("X-BCA-Timestamp", timeStamp);
				
				rest = new RestTemplate();
				entity = new HttpEntity<String>(bodyTransfer, headers);
				response = rest.postForObject(urlTransfer, entity,String.class);
				LOGGER.info("response body Transfer -> " + response);
				
				gson = new Gson(); // Or use new GsonBuilder().create();
				IDSResponse responseTransfer = gson.fromJson(response, IDSResponse.class);
				
				for(int i = 0; i < responseTransfer.getMainData().size(); i++){
					if(responseTransfer.getMainData().get(i).getKey().equals("status")){
						if(responseTransfer.getMainData().get(i).getValue().equals("TRAN SUCCESSFUL")){
							//errorSchema.setErrorCode("ESB-00-000");
							//errorSchema.setErrorMessage(responseTransfer.getMainData().get(i).getValue());
							LOGGER.info("Berhasil Transfer Dana");
						}
						else{
							throw new Exception(responseTransfer.getMainData().get(i).getValue() + ", ");
						}
					}
					else if(responseTransfer.getMainData().get(i).getKey().equals("errorMessage")){
						throw new Exception(responseTransfer.getMainData().get(i).getValue() + ", ");
					}
				}
				
				//conn.commit();
				
				rest = new RestTemplate();
				LOGGER.info("URL FIREBASE -> " + urlFirebase);
				LOGGER.info("trx id -> " + trxId);
				//url = urlFirebase;
				headers = new HttpHeaders();
				headers.add("Authorization", keyFirebase);
				headers.add("To", browserKey);
				headers.add("TrxID", trxId);
				headerG = new HttpEntity<>(headers);
				ResponseEntity<FirebaseResponse> responseFirebase = rest.exchange(urlFirebase, HttpMethod.GET, headerG, FirebaseResponse.class);
				
				if(responseFirebase.getBody().getSuccess() == 1){
					errorSchema.setErrorCode("ESB-00-000");
					errorSchema.setErrorMessage("Berhasil melakukan pembayaran dan update status sales dengan id trx " + trxId + " dan notifikasi Firebase berhasil transaksi sudah dimunculkan");
					LOGGER.info("Berhasil melakukan pembayaran dan update status sales dengan id trx " + trxId + " dan notifikasi Firebase berhasil transaksi sudah dimunculkan");
					conn.commit();
					LOGGER.info("Commit Database");
				}
				else{
					errorSchema.setErrorCode("ESB-99-999");
					errorSchema.setErrorMessage("Berhasil melakukan pembayaran dan update status sales dengan id trx " + trxId + " tetapi notifikasi Firebase berhasil transaksi gagal dimunculkan");
					LOGGER.info("Berhasil melakukan pembayaran dan update status sales dengan id trx " + trxId + " tetapi notifikasi Firebase berhasil transaksi gagal dimunculkan");
					conn.commit();
					LOGGER.info("Commit Database");
				}
				
			}
		} catch(Exception e) {
			
			conn.rollback();
			LOGGER.info("Rollback Database");
			
			errorSchema.setErrorCode("ESB-99-999");
			LOGGER.info("URL FIREBASE -> " + urlFirebaseFailed);
			LOGGER.info("TRXID -> " + trxId);
			rest = new RestTemplate();
			//url = urlFirebaseFailed;
			headers = new HttpHeaders();
			headers.add("Authorization", keyFirebase);
			headers.add("To", browserKey);
			headers.add("TrxID", trxId);
			headerG = new HttpEntity<>(headers);
			ResponseEntity<FirebaseResponse> responseFirebase = rest.exchange(urlFirebaseFailed, HttpMethod.GET, headerG, FirebaseResponse.class);
			
			if(responseFirebase.getBody().getSuccess() == 1){
				errorSchema.setErrorMessage(e.getMessage() + "dan berhasil memunculkan notifikasi Firebase gagal transaksi");
				LOGGER.info("Berhasil memunculkan notifikasi Firebase gagal transaksi");
			}
			else{
				errorSchema.setErrorMessage(e.getMessage() + "tetapi tidak berhasil memunculkan notifikasi Firebase gagal transaksi");
				LOGGER.info("Tidak berhasil memunculkan notifikasi Firebase gagal transaksi");
			}
			
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}
				if (rs2 != null) {
					rs2.close();
				}
				if (rs3 != null) {
					rs3.close();
				}
				if (rs4 != null) {
					rs4.close();
				}
				if (rs5 != null) {
					rs5.close();
				}
				if (rs6 != null) {
					rs6.close();
				}
				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
				if (pSt2 != null) {
					pSt2.close();
				}
				if (pSt3 != null) {
					pSt3.close();
				}
				if (pSt4 != null) {
					pSt4.close();
				}
				if (pSt5 != null) {
					pSt5.close();
				}
				if (pSt6 != null) {
					pSt6.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		return errorSchema;
	}

	public List<Sales> setSalesTemp(List<Sales> listSales, String merchantId, String createdBy){
		List<Sales> listSalesOutput = new ArrayList<Sales>();
		String errorMessage = "";
		Integer errorMessageNumb = 0;
		String trxId = "";
		boolean flagError = false;
		String spName = "INSERT_SALES_TEMP";
		String timeStamp2 = "";
		Date dateNow = Calendar.getInstance().getTime();
		
		Long duration = (long) ((7 * 3600) * 1000);
		
		dateNow.setTime(dateNow.getTime() + duration);

		String timeStamp = new SimpleDateFormat("dd-MMM-yy hh:mm:ss.SSS a").format(dateNow);
		
		LOGGER.info("TimeStamp : " + timeStamp);
		
		timeStamp2 = new SimpleDateFormat("ddMMyyHHmmssSSS").format(dateNow);
		
		trxId = merchantId + createdBy + timeStamp2;
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			for(int i = 0; i < listSales.size(); i++){
				//StoredProcedure
				callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?,?,?,?,?,?)}");
				
				callableStatement.setString(1, trxId);
				callableStatement.setString(2, merchantId);
				callableStatement.setString(3, listSales.get(i).getTotal());
				callableStatement.setString(4, listSales.get(i).getProductId());
				callableStatement.setString(5, listSales.get(i).getQty());
				callableStatement.setString(6, listSales.get(i).getHarga());
				callableStatement.setString(7, timeStamp);
				callableStatement.setString(8, createdBy);
				callableStatement.registerOutParameter(9, OracleTypes.CURSOR);
				
				callableStatement.executeUpdate();
		
				rs = (ResultSet) callableStatement.getObject(9);
				while(rs.next()){
					errorMessageNumb = rs.getInt("v_error_msg_numb");
					errorMessage = rs.getString("v_error_msg");
					LOGGER.info("ErrorMessage : " + errorMessage);
					if(errorMessageNumb != 1) flagError = true;
				}
			}
			
			
			
			Sales outputSales = new Sales();
			
			if(flagError == true) {
				outputSales.setTrxId(errorMessageNumb.toString());
				listSalesOutput.add(outputSales);
			}
			else {
				outputSales.setTrxId(trxId);
				listSalesOutput.add(outputSales);
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listSalesOutput;
	}

	public List<Sales> getSalesDetail (String trxId){
		LOGGER.info("PALING ATASSSSSSSSSSSSSSSSSSSSS -> " + trxId);
		List<Sales> listSalesOutput = new ArrayList<Sales>();
		
		/*
		String query2 = "SELECT s.trxid AS trxid, s.merchantid AS merchantid, m.merchantname AS merchantname, s.total AS total, "
				+ "s.productid AS productid, p.nama AS productname, s.qty AS qty, s.hargast AS hargast, s.tgl AS tgl, s.status AS status "
				+ "FROM sales s "
				+ "JOIN merchant m ON m.merchantid = s.merchantid "
				+ "JOIN product p ON p.productid = s.productid "
				+ "WHERE s.trxid = ? "
				+ "ORDER BY s.productid ASC";*/
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetSalesDetail2 + " ...");
			
			pSt2 = conn.prepareStatement(queryGetSalesDetail2);
			LOGGER.info("Prepared Statement dengan isi " + queryGetSalesDetail2 + " berhasil dijalanakan ...");
			
			pSt2.setString(1, trxId);
			
			rs2 = pSt2.executeQuery();
	
			while(rs2.next()){
				Sales sales = new Sales();
				
				sales.setTrxId(rs2.getString("trxid"));
				sales.setMerchantId(rs2.getString("merchantid"));
				sales.setMerchantName(rs2.getString("merchantname"));
				sales.setTotal(rs2.getString("total"));
				sales.setProductId(rs2.getString("productid"));
				sales.setProductName(rs2.getString("productname"));
				sales.setQty(rs2.getString("qty"));
				sales.setHarga(rs2.getString("hargast"));
				sales.setTgl(rs2.getString("tgl"));
				sales.setStatus(rs2.getString("status"));
				listSalesOutput.add(sales);
			}
			
			LOGGER.info("Berhasil mendapatkan data list sales");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listSalesOutput;
	}
	
	public ListSales getSalesDetailStruk (String trxId){
		LOGGER.info("PALING ATASSSSSSSSSSSSSSSSSSSSS -> " + trxId);
		List<Sales> listSales = new ArrayList<Sales>();
		ListSales listSalesOutput = new ListSales();
		String grandTotal = "";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetSalesDetail + " ...");
			
			pSt = conn.prepareStatement(queryGetSalesDetail);
			LOGGER.info("Prepared Statement dengan isi " + queryGetSalesDetail + " berhasil dijalanakan ...");
			
			pSt.setString(1, trxId);
			LOGGER.info("TRX ID DI SALES DETAIL -> " + trxId);
			rs = pSt.executeQuery();
	
			while(rs.next()){
				grandTotal = rs.getString("grandtotal");
				listSalesOutput.setGrandTotal(grandTotal);
			}
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetSalesDetail2 + " ...");
			
			pSt2 = conn.prepareStatement(queryGetSalesDetail2);
			LOGGER.info("Prepared Statement dengan isi " + queryGetSalesDetail2 + " berhasil dijalanakan ...");
			
			pSt2.setString(1, trxId);
			
			rs2 = pSt2.executeQuery();
	
			while(rs2.next()){
				Sales sales = new Sales();
				
				sales.setTrxId(rs2.getString("trxid"));
				sales.setMerchantId(rs2.getString("merchantid"));
				sales.setMerchantName(rs2.getString("merchantname"));
				sales.setTotal(rs2.getString("total"));
				sales.setProductId(rs2.getString("productid"));
				sales.setProductName(rs2.getString("productname"));
				sales.setQty(rs2.getString("qty"));
				sales.setHarga(rs2.getString("hargast"));
				sales.setTgl(rs2.getString("tgl"));
				sales.setStatus(rs2.getString("status"));
				listSales.add(sales);
			}
			
			listSalesOutput.setListSales(listSales);
			
			LOGGER.info("Berhasil mendapatkan data list sales");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listSalesOutput;
	}
	
	public List<TotalSales> getSalesTotal (String trxId){
		LOGGER.info("PALING ATASSSSSSSSSSSSSSSSSSSSS -> " + trxId);
		List<TotalSales> listTotalSales = new ArrayList<TotalSales>();
		TotalSales totalSales = new TotalSales();
		String grandTotal = "";
		String merchantName = "";
		
		/*String query = "SELECT SUM(total) AS grandtotal "
				+ "FROM sales WHERE trxid = ?";*/
		
		/*String query2 = "SELECT DISTINCT(m.merchantname) AS merchantname "
				+ "FROM sales s "
				+ "JOIN merchant m ON m.merchantid = s.merchantid "
				+ "WHERE s.trxid = ? ";*/
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetSalesDetail + " ...");
			
			pSt = conn.prepareStatement(queryGetSalesDetail);
			LOGGER.info("Prepared Statement dengan isi " + queryGetSalesDetail + " berhasil dijalanakan ...");
			
			pSt.setString(1, trxId);
			LOGGER.info("TRX ID DI SALES DETAIL -> " + trxId);
			rs = pSt.executeQuery();
	
			while(rs.next()){
				grandTotal = rs.getString("grandtotal");
				totalSales.setGrandTotal(grandTotal);
			}
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetSalesName + " ...");
			
			pSt2 = conn.prepareStatement(queryGetSalesName);
			LOGGER.info("Prepared Statement dengan isi " + queryGetSalesName + " berhasil dijalanakan ...");
			
			pSt2.setString(1, trxId);
			
			rs2 = pSt2.executeQuery();
	
			while(rs2.next()){
				merchantName = rs2.getString("merchantname");
				totalSales.setMerchantName(merchantName);
			}
			
			listTotalSales.add(totalSales);
			
			LOGGER.info("Berhasil mendapatkan data total sales");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}
				if (rs2 != null) {
					rs2.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
				if (pSt2 != null) {
					pSt2.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listTotalSales;
	}
	
	public List<Sales> getSalesStatus (String trxId){
		List<Sales> listSalesOutput = new ArrayList<Sales>();
		
		/*String query = "SELECT DISTINCT(status) "
				+ "FROM sales WHERE trxid = ? ";*/
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetSalesStatus + " ...");
			
			pSt = conn.prepareStatement(queryGetSalesStatus);
			LOGGER.info("Prepared Statement dengan isi " + queryGetSalesStatus + " berhasil dijalanakan ...");
			
			pSt.setString(1, trxId);
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				Sales sales = new Sales();
				
				sales.setStatus(rs.getString("status"));
				listSalesOutput.add(sales);
			}
			LOGGER.info("Berhasil mendapatkan data sales");
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listSalesOutput;
	}

	public ErrorSchema updateSalesStatus (String trxId, String customerId){
		ErrorSchema errorSchema = new ErrorSchema();
		String errorMessage = "";
		Integer errorMessageNumb = 0;
		String spName = "UPDATE_SALES_STATUS";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//StoredProcedure
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");
			
			callableStatement.setString(1, customerId);
			callableStatement.setString(2, trxId);
			callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
			LOGGER.info("Menjalankan registerOutParameter untuk Cursor dengan value -> " + OracleTypes.CURSOR + " ...");
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
	
			rs = (ResultSet) callableStatement.getObject(3);
			while(rs.next()){
				errorMessageNumb = rs.getInt("v_error_msg_numb");
				errorMessage = rs.getString("v_error_msg");
			}
			
			if(errorMessageNumb == 1) {
				errorSchema.setErrorCode("ESB-00-00");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.info("Berhasil melakukan update status sales dengan id trx " + trxId + " ...");
			}
			else {
				errorSchema.setErrorCode("ESB-99-999");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.error("Gagal melakukan update ...");
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return errorSchema;
	}

	@Override
	public List<Sales> testSelect() {
		List<Sales> listSales = new ArrayList<Sales>();
		Sales sales = new Sales();
			
		try{
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			
			pSt = conn.prepareStatement(queryTest);
			LOGGER.info("Prepared Statement dengan isi " + queryTest + " berhasil dijalanakan ...");
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				sales = new Sales();
				
				sales.setTrxId(rs.getString("trxid"));
				sales.setMerchantId(rs.getString("merchantid"));
				sales.setHarga(rs.getString("hargast"));
				sales.setStatus(rs.getString("status"));
				listSales.add(sales);
			}
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listSales;
	}
}
