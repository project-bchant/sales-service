package com.bca.bchant.salesservice.dao;

import java.sql.SQLException;
import java.util.List;

import com.bca.bchant.salesservice.model.ErrorSchema;
import com.bca.bchant.salesservice.model.ListSales;
import com.bca.bchant.salesservice.model.TotalSales;
import com.bca.bchant.salesservice.model.Sales;

public interface SalesDao {

	ErrorSchema setSales(String trxId, String customerId, String browserKey) throws SQLException;
	List<Sales> setSalesTemp (List<Sales> listSales, String merchantId, String createdBy);
	List<Sales> getSalesDetail (String trxId);
	ListSales getSalesDetailStruk (String trxId);
	List<TotalSales> getSalesTotal (String trxId);
	List<Sales> getSalesStatus (String trxId);
	ErrorSchema	updateSalesStatus (String trxId, String customerId);
	List<Sales> testSelect();
}
