package com.bca.bchant.salesservice.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.bchant.salesservice.dao.SalesDao;
import com.bca.bchant.salesservice.model.ErrorSchema;
import com.bca.bchant.salesservice.model.ListSales;
import com.bca.bchant.salesservice.model.TotalSales;
import com.bca.bchant.salesservice.model.Sales;

@Service
public class SalesServiceImpl implements SalesService{
	
	@Autowired
	SalesDao salesDao;
	
	@Override
	public ErrorSchema setSales (String trxId, String customerId, String browserKey) throws SQLException{
		return this.salesDao.setSales(trxId, customerId, browserKey);
	}
	
	@Override
	public List<Sales> setSalesTemp (List<Sales> listSales, String merchantId, String createdBy){
		return this.salesDao.setSalesTemp(listSales, merchantId, createdBy);
	}
	
	@Override
	public List<Sales> getSalesDetail (String trxId){
		return this.salesDao.getSalesDetail(trxId);
	}
	
	@Override
	public ListSales getSalesDetailStruk (String trxId){
		return this.salesDao.getSalesDetailStruk(trxId);
	}
	
	@Override
	public List<TotalSales> getSalesTotal (String trxId){
		return this.salesDao.getSalesTotal(trxId);
	}
	
	@Override
	public List<Sales> getSalesStatus (String trxId){
		return this.salesDao.getSalesStatus(trxId);
	}
	
	@Override
	public ErrorSchema	updateSalesStatus (String trxId, String customerId){
		return this.salesDao.updateSalesStatus(trxId, customerId);
	}

	@Override
	public List<Sales> testSelect() {
		// TODO Auto-generated method stub
		return this.salesDao.testSelect();
	}
}
