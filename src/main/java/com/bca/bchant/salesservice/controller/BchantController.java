package com.bca.bchant.salesservice.controller;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bca.bchant.salesservice.model.ErrorSchema;
import com.bca.bchant.salesservice.model.ListSales;
import com.bca.bchant.salesservice.model.TotalSales;
import com.bca.bchant.salesservice.model.Sales;
import com.bca.bchant.salesservice.service.SalesService;

@CrossOrigin(origins = "*")
@RestController
public class BchantController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BchantController.class);
	
	@Autowired
	SalesService salesService;
	
	@RequestMapping(value = "/testSelect", method = RequestMethod.GET)
    public ResponseEntity<List<Sales>> testSelect() {
		LOGGER.info("Memanggil service testSelect ... ");
		List<Sales> listSales = this.salesService.testSelect();
		
		return new ResponseEntity<List<Sales>>(listSales, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/setSales", method = RequestMethod.POST)
    public ResponseEntity<ErrorSchema> setSales(@RequestHeader("TrxID") String trxId, @RequestHeader("CustomerID") String customerId, @RequestHeader("BrowserKey") String browserKey) throws SQLException {
    	LOGGER.info("Memanggil service setSales ...");
    	ErrorSchema errorSchema = this.salesService.setSales(trxId, customerId, browserKey);
        
		return new ResponseEntity<ErrorSchema>(errorSchema, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/setSalesTemp", method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Sales>> setSalesTemp(@RequestBody List<Sales> listSales, @RequestHeader("MerchantID") String merchantId,  @RequestHeader("CreatedBy") String createdBy) {
    	LOGGER.info("Memanggil service setSalesTemp ...");
    	List<Sales> listSalesOutput = this.salesService.setSalesTemp(listSales, merchantId, createdBy);
        
		return new ResponseEntity<List<Sales>>(listSalesOutput, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/getSalesTotal", method = RequestMethod.GET)
    public ResponseEntity<List<TotalSales>> getSalesTotal(@RequestHeader("TrxID") String trxId) {
		LOGGER.info("Memanggil service getSalesTotal ... ");
		List<TotalSales> listTotalSales = this.salesService.getSalesTotal(trxId);
		
		return new ResponseEntity<List<TotalSales>>(listTotalSales, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getSalesDetail", method = RequestMethod.GET)
    public ResponseEntity<List<Sales>> getSalesDetail(@RequestHeader("TrxID") String trxId) {
		LOGGER.info("Memanggil service getSalesDetail ... ");
		List<Sales> listSalesOutput = this.salesService.getSalesDetail(trxId);
		
		return new ResponseEntity<List<Sales>>(listSalesOutput, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getSalesDetailStruk", method = RequestMethod.GET)
    public ResponseEntity<ListSales> getSalesDetailStruk(@RequestHeader("TrxID") String trxId) {
		LOGGER.info("Memanggil service getSalesDetailStruk ... ");
		ListSales listSalesOutput = this.salesService.getSalesDetailStruk(trxId);
		
		return new ResponseEntity<ListSales>(listSalesOutput, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getSalesStatus", method = RequestMethod.GET)
    public ResponseEntity<List<Sales>> getSalesStatus(@RequestHeader("TrxID") String trxId) {
		LOGGER.info("Memanggil service getSalesStatus ... ");
		List<Sales> listSalesOutput = this.salesService.getSalesStatus(trxId);
		
		return new ResponseEntity<List<Sales>>(listSalesOutput, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/updateSalesStatus", method = RequestMethod.POST)
    public ResponseEntity<ErrorSchema> updateSalesStatus(@RequestHeader("TrxID") String trxId, @RequestHeader("CustomerID") String customerId) {
    	LOGGER.info("Memanggil service updateSalesStatus ...");
    	ErrorSchema errorSchema = this.salesService.updateSalesStatus(trxId, customerId);
    	
		return new ResponseEntity<ErrorSchema>(errorSchema, HttpStatus.OK);
    }
	
}
