package com.bca.bchant.salesservice.model;

import java.util.ArrayList;
import java.util.List;

public class FirebaseResponse {
	Long multicast_id;
	Integer success;
	Integer failure;
	Integer canonical_ids;
	List<FirebaseResultResponse> results = new ArrayList<FirebaseResultResponse>();

	public Long getMulticast_id() {
		return multicast_id;
	}

	public void setMulticast_id(Long multicast_id) {
		this.multicast_id = multicast_id;
	}

	public Integer getSuccess() {
		return success;
	}

	public void setSuccess(Integer success) {
		this.success = success;
	}

	public Integer getFailure() {
		return failure;
	}

	public void setFailure(Integer failure) {
		this.failure = failure;
	}

	public Integer getCanonical_ids() {
		return canonical_ids;
	}

	public void setCanonical_ids(Integer canonical_ids) {
		this.canonical_ids = canonical_ids;
	}

	public List<FirebaseResultResponse> getResults() {
		return results;
	}

	public void setResults(List<FirebaseResultResponse> results) {
		this.results = results;
	}

	
}