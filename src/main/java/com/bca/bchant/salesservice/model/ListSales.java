package com.bca.bchant.salesservice.model;

import java.util.ArrayList;
import java.util.List;

public class ListSales {
	private String grandTotal;
	private List<Sales> listSales = new ArrayList<Sales>();
	
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	public List<Sales> getListSales() {
		return listSales;
	}
	public void setListSales(List<Sales> listSales) {
		this.listSales = listSales;
	}
}
