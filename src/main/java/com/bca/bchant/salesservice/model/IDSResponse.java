package com.bca.bchant.salesservice.model;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class IDSResponse {
	private List<KeyValue> MainData = new ArrayList<KeyValue>();
    private Map<String,List<AdditionalDataResponse>> AdditionalData = new Hashtable<String,List<AdditionalDataResponse>>();
    private String ResponseCode="";
    
	public List<KeyValue> getMainData() {
		return MainData;
	}
	public void setMainData(List<KeyValue> mainData) {
		MainData = mainData;
	}
	public Map<String, List<AdditionalDataResponse>> getAdditionalData() {
		return AdditionalData;
	}
	public void setAdditionalData(Map<String, List<AdditionalDataResponse>> additionalData) {
		AdditionalData = additionalData;
	}
	public String getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}
	@Override
	public String toString() {
		return "IDSResponse [MainData=" + MainData + ", AdditionalData=" + AdditionalData + ", ResponseCode="
				+ ResponseCode + "]";
	}

    
	
    
    
}
